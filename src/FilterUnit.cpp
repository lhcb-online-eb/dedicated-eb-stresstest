#include <mpi.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <malloc.h>
#include <assert.h>
#include <ctime>
#include "../inc/Unit.hpp"
#include "../inc/FilterUnit.hpp"
#include <time.h>
#include "../inc/common.hpp"
#include "string.h"
#include <stdlib.h>
#include <unistd.h>

FilterUnit :: FilterUnit(int rank):Unit(rank)
{

	this->dataBuffer = new char*[BUFFER_COUNT];

	//this->fuReadyRequests=new MPI_Request[FU_MAX_TRANSMISSIONS_ON_THE_GO];
	//this->dataSendRequests=new MPI_Request[FU_MAX_TRANSMISSIONS_ON_THE_GO];

	//this->fuReadyStatus=new MPI_Status[FU_MAX_TRANSMISSIONS_ON_THE_GO];
	//this->dataSendStatus=new MPI_Status[FU_MAX_TRANSMISSIONS_ON_THE_GO];

	this->fuReadyRequests=MPI_REQUEST_NULL;
	
	//this->dataSendRequests=MPI_REQUEST_NULL;
	
	
	this->dataSentRequests= new MPI_Request[FU_ON_THE_GO_SENDS];
	
	
	for (unsigned int i = 0 ; i < FU_ON_THE_GO_SENDS; i++) 
	{
		dataSentRequests[i] = MPI_REQUEST_NULL;
		//dataBuffer[i] = (char*)malloc(BUFFER_SIZE_MB*1024*1024);
	}
	
	transmittedChunks=0;

	//this->fuReadyMessage = new MpiRuReadyRequest[totalProcessesNo];

	for (unsigned int i = 0 ; i < BUFFER_COUNT ; i++) 
	{
		dataBuffer[i] = (char*)memalign(2*1024*1024,BUFFER_SIZE_MB*1024*1024);
		//dataBuffer[i] = (char*)malloc(BUFFER_SIZE_MB*1024*1024);
	}

	//initialize requests structs
	/*
	for (unsigned int j = 0 ; j < FU_MAX_TRANSMISSIONS_ON_THE_GO ; j++)
	{
		reqRecvRequests[j]=MPI_REQUEST_NULL;
	}
	*/
	/*
	for (int j = 0 ; j < FU_MAX_TRANSMISSIONS_ON_THE_GO ; j++)
	{
		dataSendRequests[j]=MPI_REQUEST_NULL;
	}
	*/
	fuReadyMessage.rank=rank;


}

	
void FilterUnit :: run()
{
	int64_t now,start, global_start;
	start = g_get_real_time();
	global_start=start;
	long int us_s=1e6;
	
	
	printf("FU RANK: %d commencing operations  \n",this->rank);

	signalReadiness();
	initRecvFromBu();

	//for( int a = 0 ;a != FU_ON_THE_GO_SENDS; a++)
	//	signalReadiness();



	//printf("FU RANK: %d READINESS SIGNALLED  \n",this->rank);

	//todo - commenting this lane makes everything work ok

	MPI_Barrier(MPI_COMM_WORLD);
	while(true)
	{
		
		//usleep(1000);
		//for(int i=0; i!=5;i++)


		
		//MPI_Wait(&dataSentRequests, MPI_STATUS_IGNORE);
		
		//signalReadiness();
		//transmittedChunks++;
		
		bool data_transmission_complete = false;
		while(data_transmission_complete !=true)
		{
				int indx;

				data_transmission_complete=probeForCompletion(&indx);
				if(data_transmission_complete==true)
				{
					//printf("FU RANK: %d RECEIVING DATA TRANSMISSION FINISHED  \n",this->rank);
					signalReadiness();
					transmittedChunks++;
					//recvFromBu();
					recvFromBuAtInd(indx);
				}
		}
		
		now = g_get_real_time();
		if((now-start)>us_s*PROBE_TP_LAPSE)
		{

			long int bytes=BUFFER_SIZE_MB*1024*1024*transmittedChunks;
			//printf("TIME: now: %ld, global start %ld elapsed %ld\n",now/us_s,global_start/us_s,(now-global_start)/us_s);
			double tput_bytes_per_sec = (double)bytes * us_s / ((double)(now - start));
			double tput_gbits_per_sec = tput_bytes_per_sec/1024.0/1024.0/128.0;
			printf("[ %d s ]THROUGHPUT FOR FILTER RANK %d: %lf Gbits/s\n",(now-global_start)/us_s,rank, tput_gbits_per_sec);
			transmittedChunks=0;
			start=now;		
		}
		
		
	}	

};


void FilterUnit :: signalReadiness()
{

	MPI_Wait(&(fuReadyRequests), MPI_STATUS_IGNORE);
	MPI_Isend(&(fuReadyMessage),sizeof(MpiFuReadyMsg),MPI_CHAR,0,rank,MPI_COMM_WORLD,&(fuReadyRequests));
	//printf("FU rank %d signalled readiness \n",this->rank);
	//MpiFuReadyRequest fuReadyMesg;
	
	//MPI_Send(&(fuReadyMessage),sizeof(MpiFuReadyRequest),MPI_CHAR,0,rank,MPI_COMM_WORLD);
	//printf("FU rank %d DONE SENDING \n",this->rank);
};

bool FilterUnit :: probeForCompletion(int *indx)
{
	int dataReceived=0;
	//printf("FU probing for completion: running TEST\n");
	//MPI_Test(&dataSendRequests, &dataReceived, MPI_STATUS_IGNORE);

	//int MPI_Testany(int count, MPI_Request array_of_requests[], int *indx, int *flag, MPI_Status *status)

	MPI_Testany(FU_ON_THE_GO_SENDS, dataSentRequests, indx, &dataReceived ,MPI_STATUS_IGNORE);
	
	return dataReceived;
};
/*
void FilterUnit :: recvFromBu()
{
	MPI_Wait(&(dataSendRequests), MPI_STATUS_IGNORE);
	unsigned int ind =  getNextBuffIndex();
	MPI_Irecv((dataBuffer[ind]),BUFFER_SIZE_MB*1024*1024,MPI_CHAR,MPI_ANY_SOURCE,MPI_ANY_TAG,MPI_COMM_WORLD,&(dataSendRequests));
}
*/


void FilterUnit :: recvFromBuAtInd(int index)
{
	MPI_Wait(&(dataSentRequests[index]), MPI_STATUS_IGNORE);
	unsigned int ind =  getNextBuffIndex();
	MPI_Irecv((dataBuffer[ind]),BUFFER_SIZE_MB*1024*1024,MPI_CHAR,MPI_ANY_SOURCE,MPI_ANY_TAG,MPI_COMM_WORLD,&(dataSentRequests[index]));
}


void FilterUnit :: initRecvFromBu()
{
	//MPI_Wait(&(dataSendRequests), MPI_STATUS_IGNORE);
	//unsigned int ind =  getNextBuffIndex();
	//MPI_Irecv((dataBuffer[ind]),BUFFER_SIZE_MB*1024*1024,MPI_CHAR,MPI_ANY_SOURCE,MPI_ANY_TAG,MPI_COMM_WORLD,&(dataSendRequests));
	
	for( int i=0; i != FU_ON_THE_GO_SENDS; i++ )
	{
		MPI_Wait(&(dataSentRequests[i]), MPI_STATUS_IGNORE);
		unsigned int ind =  getNextBuffIndex();
		//printf("FU: getting data out of index %d\n", ind);
		MPI_Irecv((dataBuffer[ind]),BUFFER_SIZE_MB*1024*1024,MPI_CHAR,MPI_ANY_SOURCE,MPI_ANY_TAG,MPI_COMM_WORLD,&(dataSentRequests[i]));
	}
	
};


