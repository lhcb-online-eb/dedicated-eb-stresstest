#include <mpi.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <malloc.h>
#include <assert.h>
#include <ctime>
#include "../inc/common.hpp"
#include "../inc/Unit.hpp"
#include <glib.h>
#include <limits>
#include <thread>


Unit :: Unit(int rank)
{
	this->rank=rank;
	currentBuffInd=0;
};

void Unit :: run()
{
};


unsigned int Unit :: getNextBuffIndex()
{
	currentBuffInd++;
	
	if(currentBuffInd>=BUFFER_COUNT)
		currentBuffInd=0;
	
	return currentBuffInd;
}
