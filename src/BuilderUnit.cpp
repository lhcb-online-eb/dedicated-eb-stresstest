#include <mpi.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <malloc.h>
#include <assert.h>
#include <ctime>
#include "../inc/BuilderUnit.hpp"
#include <time.h>
#include "../inc/common.hpp"
#include <sys/timerfd.h>
#include <glib.h>

BuilderUnit :: BuilderUnit(int rank):Unit(rank)
{
	this->dataBuffer = new char*[BUFFER_COUNT];
	
	this->transmissionRunningMap = new bool [BUFFER_COUNT];
	
	for (unsigned int i = 0 ; i < BUFFER_COUNT ; i++) 
	{
		dataBuffer[i] = (char*)memalign(2*1024*1024,BUFFER_SIZE_MB*1024*1024);
		//dataBuffer[i] = (char*)malloc(BUFFER_SIZE_MB*1024*1024);
	}
	
	this->delayTimer = 0;
	this->now = g_get_real_time();
	this->start = g_get_real_time();
	this->totalEventsOnQueue = 0;

	this->dataTransmissionRequests=new MPI_Request[MAX_BU_SENDS];
	
	for(int y=0;y!=MAX_BU_SENDS;y++)
	{
		this->dataTransmissionRequests[y] = MPI_REQUEST_NULL;
		this->transmissionRunningMap[y] = false;
	}
	
	this->buReadyRequests = MPI_REQUEST_NULL;
	this->transGrantedRequests = MPI_REQUEST_NULL;
	this->runningBuRuTransmissions = 0;

};


void BuilderUnit :: run()
{

	startReceiveFromMU();
	printf("BU commencing operations RANK: %d \n",this->rank);

	//for(int i = 0; i != eventsGeneratedPerBu; i++)
	//{

	MPI_Barrier(MPI_COMM_WORLD);
	while(true)
	{
		if(timeElapsedCheck() == true)
		{
			bool queueFull = generateEventData();
			//if(queueFull != true)
			signalEventsReady();
		}
		
		int ready_rank=0;
		if (checkIfGrantedTransmission(&ready_rank) == true)
		{
			//printf("BU RANK %d GRANTING TRANSMISSION to RANK %d:  TOTAL EVENTS GENERATED NOW  %d\n",this->rank,ready_rank, this->totalEventsOnQueue);
			sendDataToFu(ready_rank);
			startReceiveFromMU();
		}
	}
	//}
};


bool BuilderUnit :: generateEventData()
{
	if(totalEventsOnQueue == BUFFER_COUNT)
	{	
		//printf("BU RANK: %d QUEUE IF FULL. CANNOT GENERATE MORE  \n",this->rank);
		return true;
	}
	
	//MPI_Isend(&(fuReadyMessage),sizeof(MpiFuReadyRequest),MPI_CHAR,0,rank,MPI_COMM_WORLD,&(fuReadyRequests));
	totalEventsOnQueue++;
	return false;
	//printf("BU RANK %d:  TOTAL EVENTS GENERATED NOW  %d\n",this->rank, this->totalEventsOnQueue);
	//TODO make searching the entries

};

void BuilderUnit :: sendDataToFu(int rank)
{
		
	int trans_index = getNextFreeTransmissionIndex();
	transmissionRunningMap[trans_index] = true;
	//printf("BU RANK: %d SENDING SLOT %d to rank %d \n", this->rank, trans_index,rank);
	MPI_Wait(&(dataTransmissionRequests[trans_index]), MPI_STATUS_IGNORE);

	unsigned int ind =  getNextBuffIndex();
	//printf("BU: transmitting under index %d and free request index %d\n", ind, trans_index);

	MPI_Isend((dataBuffer[ind]),BUFFER_SIZE_MB*1024*1024,MPI_CHAR,rank,this->rank,MPI_COMM_WORLD,&(dataTransmissionRequests[trans_index]));
	//printf("BU RANK: %d INITIALIZED SEND TO RU %d  \n",this->rank, rank);
};

void BuilderUnit :: signalEventsReady()
{
	buReadyMessage.rank = this->rank;
	buReadyMessage.currentQueueLength = this->totalEventsOnQueue;
	
	MPI_Wait(&(buReadyRequests), MPI_STATUS_IGNORE);
	MPI_Isend(&(buReadyMessage),sizeof(MpiBuReadyMsg),MPI_CHAR,0,rank,MPI_COMM_WORLD,&(buReadyRequests));
};

bool BuilderUnit :: timeElapsedCheck()
{
	delayTimer++;
		if(delayTimer == PROBE_EVERY_IT)
		{
			delayTimer = 0;
			now=g_get_real_time();	
			if(now >= start + TIME_ELAPSED_FOR_GENERATION)
			{
				start = now;
				return true;
			}
		}
	return false;
	
};


bool BuilderUnit :: checkIfGrantedTransmission(int *readyRank)
{
	int dataReceived;
	
	MPI_Test(&transGrantedRequests, &dataReceived, MPI_STATUS_IGNORE);
	
	if(dataReceived == true)
		*readyRank = transGrantedMessage.sendToWhichFu;
	
	return dataReceived;
	//int MPI_Test(MPI_Request *request, int *flag, MPI_Status *status)
	//MPI_Testany(howManyBus,buReadyRequests,&recv_index, &recv_flag,buReadyStatus);	
	
};

void BuilderUnit :: startReceiveFromMU()
{
		MPI_Wait(&(transGrantedRequests), MPI_STATUS_IGNORE);
		MPI_Irecv(&transGrantedMessage,sizeof(MpiBuTeansmissionGrantMsg),MPI_CHAR,0,0,MPI_COMM_WORLD,&(transGrantedRequests));
};


int BuilderUnit :: getNextFreeTransmissionIndex()
{
	for(int i = 0; i != MAX_BU_SENDS; i++)
	{
		if (transmissionRunningMap[i] == false)
		return i;
	}
	
	int id;
	int ret = MPI_Waitany(MAX_BU_SENDS,dataTransmissionRequests,&id,MPI_STATUS_IGNORE);
	transmissionRunningMap[id] = false;
	dataTransmissionRequests[id] = MPI_REQUEST_NULL;
	
	return ret;
};
