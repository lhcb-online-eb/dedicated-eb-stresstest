#include <mpi.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <malloc.h>
#include <assert.h>
#include <ctime>
#include "../inc/Unit.hpp"
#include "../inc/ManagerUnit.hpp"
#include "../inc/BuilderUnit.hpp"
#include "../inc/FilterUnit.hpp"
#include "../inc/common.hpp"
#include <glib.h>
#include <limits>
#include <thread>

void parseParams(int argc, char ** argv)
{
	int c;
	while ((c = getopt (argc, argv, "E:F:t:h")) != -1)
    	{
		switch (c)
		{
			case 'h':
				fprintf(stderr,"%s [-m {BUFFER SIZE in KB} -F {how many FUs} -t {timeout in seconds} -E {how many events generated for each of the BUs}]\n",argv[0]);
				exit(EXIT_SUCCESS);
			case 'F':
				howManyFus = atoi(optarg);;
				break;
			case 't':
				timeoutSeconds = atoi(optarg);;
				break;
			case 'E':
				eventsGeneratedPerBu = atoi(optarg);;
				break;			
			default:
				fprintf(stderr,"Invalid options : %c\n",c);
				abort ();
		}
	}
}

void runTransmissions(Unit * unit)
{
	unit->run();
}


int main(int argc, char** argv) 
{


	parseParams(argc,argv);
	//initialize
	MPI_Init(NULL, NULL);

	
	MPI_Comm_size(MPI_COMM_WORLD, &totalProcessesNo);
	int rank;
	MPI_Comm_rank(MPI_COMM_WORLD, &rank);

  
	char host_name[MPI_MAX_PROCESSOR_NAME];
	int name_len;
	MPI_Get_processor_name(host_name, &name_len);
	

	printf("PROCESS ON HOST %s, RANK %d\n", host_name, rank);



	Unit * unit_ptr;
	howManyBus=totalProcessesNo-howManyFus-1;
	if(rank==0)
	{
		printf("Number of processes: %d, number of FUs: %d, number of BUs: %d \n",totalProcessesNo, howManyFus, howManyBus);
		printf("BUFFER/MESSAGE size in MB: %d \n",BUFFER_SIZE_MB);
	}



	if(rank==0)
	{
		unit_ptr = new ManagerUnit (rank, howManyFus, howManyBus);
	}
	else if(rank<=howManyBus)
	{
		unit_ptr = new BuilderUnit (rank);
	}
	else
	{
		unit_ptr = new FilterUnit (rank);
	}

/*	
	if(rank==0)
	{
		for ( int i = 0 ; i < howManyFus ; i++)
		{
			MPI_Status aStatus;
			int fuReadyMessage;
			printf("MU initializing receive for fu rank  %d and index %d\n",i+1, i);
			MPI_Recv(&fuReadyMessage,sizeof(int),MPI_CHAR,i+1,MPI_ANY_TAG,MPI_COMM_WORLD,&aStatus);	
			//MPI_Irecv(&(fuReadyMessages[i]),sizeof(MpiFuReadyRequest),MPI_CHAR,fusRanks[i],MPI_ANY_TAG,MPI_COMM_WORLD,&(fuReadyRequests[i]));		
		}
		printf("MU done\n");	
	}
	else
	{
		printf("FU rank %d signalling readiness \n",rank);
		//MPI_ISend(&(fuReadyMessage),sizeof(MpiFuReadyRequest),MPI_CHAR,0,rank,MPI_COMM_WORLD,&(fuReadyRequests));
		int fuReadyMesg;
	
		MPI_Send(&(fuReadyMesg),sizeof(int),MPI_CHAR,0,rank,MPI_COMM_WORLD);
		printf("FU rank %d DONE SENDING \n",rank);
	}
*/
////////////////	
/*
	MpiFuReadyRequest tst[howManyFus];
	MPI_Status aStatus;
	MPI_Request reqs[howManyFus];

	if(rank==0)
	{
		for (int a=0;a!=howManyFus;a++)
		{
			printf("TEST RANK: %d \n", rank);
			MPI_Irecv(&(tst[a]),sizeof(MpiFuReadyRequest),MPI_CHAR,a+1,MPI_ANY_TAG,MPI_COMM_WORLD,&(reqs[a]));
			printf("DONE AFTER A SIMPLE RECV: %d from rank %d\n", rank, a);
		}
		for (int a=0;a!=howManyFus;a++)
		{
			MPI_Wait(&(reqs[a]),&aStatus);
			printf("GOT A MESSAGE FROM RANK: %d \n", a+1);
		}
		
	}
	else if (rank>=1)
	{
		printf("TEST RANK: %d \n", rank);
		MPI_Isend(&(tst[0]),sizeof(MpiFuReadyRequest),MPI_CHAR,0,rank,MPI_COMM_WORLD,&(reqs[0]));
		//MPI_Send(&tst,4,MPI_CHAR,0,rank,MPI_COMM_WORLD);
		printf("DONE AFTER A SIMPLE SEND: %d \n", rank);
	}
*/


	std::thread watchdog([]()
	{
		std::this_thread::sleep_for(std::chrono::seconds(timeoutSeconds));
		printf("Timeout %d reached, exiting \n", timeoutSeconds);
		exit(1);
	});
	watchdog.detach();	
	
	
	MPI_Barrier(MPI_COMM_WORLD);

	runTransmissions(unit_ptr);

	//MPI_Barrier(MPI_COMM_WORLD);
	
	MPI_Finalize();

}
