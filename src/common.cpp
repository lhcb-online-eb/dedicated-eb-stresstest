#include <sys/timerfd.h>
#include "../inc/common.hpp"

int howManyFus = 1;
int howManyBus = 1;
int howManyProcesses = 1;

size_t BUFFER_SIZE_MB = 1;

int timeoutSeconds = 30;

unsigned int eventsGeneratedPerBu = 4096;

int fuToBuRatio = 4;

int totalProcessesNo=6;
