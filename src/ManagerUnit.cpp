#include <mpi.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <malloc.h>
#include <assert.h>
#include <ctime>
#include "../inc/Unit.hpp"
#include "../inc/ManagerUnit.hpp"
#include "../inc/common.hpp"
#include <glib.h>
#include <limits>
#include <thread>

ManagerUnit :: ManagerUnit(int rank):Unit(rank)
{

};

ManagerUnit :: ManagerUnit(int rank,int howManyFus,int howManyBus):Unit(rank)
{
	this->howManyBus=howManyBus;
	this->howManyFus=howManyFus;
	
	//printf("MU: howManyBus %d \n",howManyBus);
	//printf("MU: howManyFus %d \n",howManyFus);

	this->busRanks = new int[howManyBus];
	this->fusRanks = new int[howManyFus];
	
	this->busRanksReady = new int[howManyBus];
	this->fusRanksReady = new int[howManyFus];

	this->fuReadyRequests = new MPI_Request[howManyFus];
	this->buReadyRequests = new MPI_Request[howManyBus];
	//fuReadyStatus = new MPI_Status[howManyFus];
	//buReadyStatus = new MPI_Status[howManyBus];
		
	this->fuReadyMessages = new MpiFuReadyMsg[howManyFus];
	this->buReadyMessages = new MpiBuReadyMsg[howManyBus];
	
	this->transGrantMessages = new MpiBuTeansmissionGrantMsg[howManyBus];
	this->transGrantRequests = new MPI_Request[howManyBus];
	//transGrantStatus = new MPI_Status[howManyBus];


	this->servicedBuTransmissions = 0;
	
	int rankIt;
	for(rankIt = 0;rankIt != howManyBus; rankIt++)
	{
		this->busRanksReady[rankIt] = 0;
		this->busRanks[rankIt] = rankIt + 1;
		//printf("MU: Assigned BU rank %d for the index %d \n",this->busRanks[rankIt],rankIt);
		this->transGrantRequests[rankIt] = MPI_REQUEST_NULL;
		this->buReadyRequests[rankIt] = MPI_REQUEST_NULL;
	}
	
	for(rankIt = 0; rankIt != howManyFus; rankIt++)
	{
		fuReadyRequests[rankIt] = MPI_REQUEST_NULL;
		this->fusRanksReady[rankIt] = 0;
		this->fusRanks[rankIt] = rankIt + 1 + howManyBus;
		//printf("MU: Assigned FU rank %d for the index %d \n",this->fusRanks[rankIt],rankIt);
	}	
	


	
	
};
	
void ManagerUnit :: run()
{
	printf("MU commencing operations RANK: %d \n",this->rank);
	initializeReceivingFus();
	initializeReceivingBus();
	

	MPI_Barrier(MPI_COMM_WORLD);
	while(true)
	{
		getReadyFus();
		getEventsBus();
		if(servicedBuTransmissions < MAX_BU_SENDS)
			grantTransmissionsToBus();
	}
	
	
};

void ManagerUnit :: getReadyFus()
{
	
	//printf("MU getting ready FUs\n");
	
	int recv_flag = false;
	int recv_index;
	int how_many_ready = 0;
	int array_of_indices[howManyFus];
	MPI_Status array_of_statuses[howManyFus];
	//printf("MU rank %d waiting for any FUs ready\n",this->rank);
	//MPI_Waitany(howManyFus,fuReadyRequests,&request_ind,fuReadyStatus);	

	//printf("MU rank %d GOT A MESSAGE\n",this->rank);
	
	MPI_Testany(howManyFus,fuReadyRequests,&recv_index, &recv_flag,MPI_STATUS_IGNORE);	
	
	//MPI_Testsome(howManyFus, fuReadyRequests, &how_many_ready,array_of_indices, array_of_statuses);
	
	//MPI_Waitall(howManyFus, fuReadyRequests,fuReadyStatus);
	//if(how_many_ready>0)
	//	printf("READY FUS: %d \n", how_many_ready);
	
	if(recv_flag==true)
	{

		
		MPI_Wait(&(fuReadyRequests[recv_index]), MPI_STATUS_IGNORE);
		MPI_Irecv(&(fuReadyMessages[recv_index]),sizeof(MpiFuReadyMsg),MPI_CHAR,fusRanks[recv_index],fusRanks[recv_index],MPI_COMM_WORLD,&(fuReadyRequests[recv_index]));	
		fusRanksReady[recv_index]++;
		servicedBuTransmissions--;

		//printf("MU ready FU under index %d corresponding to rank %d. RANK FROM MESSAGE: %d. fusRanksReady[y]: %d \n",recv_index, fusRanks[recv_index], fuReadyMessages[recv_index].rank, fusRanksReady[recv_index]);

		//for(int y=0;y!=howManyFus;y++)
		//	printf("MU ready slots for RU rank %d: %d \n",fusRanks[y], fusRanksReady[y]);	
	}
	
	
	/*
	MPI_Status aStatus;
	for(int a=0;a!=howManyFus;a++)
	{
		MPI_Wait(&(fuReadyRequests[a]),&aStatus);
		printf("GOT %d out of %d \n",a,howManyFus);
	}
	*/
	
};
		
void ManagerUnit :: grantTransmissionsToBus()
{
	//first fit naive approach
	for(int x = 0 ;x != howManyBus; x++)
	{
		if(busRanksReady[x] > 0)
		{
			for(int y = 0; y != howManyFus; y++)
			{
				if(fusRanksReady[y] > 0 && busRanksReady[x] > 0)
				{
					busRanksReady[x]--;
					fusRanksReady[y]--;
					//printf("MU: Sending a send request for BU rank %d and RU rank %d. Remaining BUs events: %d Remaining Fus tokens: %d \n",busRanks[x],fusRanks[y],busRanksReady[x],fusRanksReady[y]);
					sendBusTransmisisonGrant(x,y);
					break;
				}
			}
		}
	}
};

void ManagerUnit :: initializeReceivingBus()
{

	for ( int i = 0; i < howManyBus ; i++)
	{
		//printf("MU initializing receive for bu rank  %d\n",busRanks[i]);
		MPI_Wait(&(buReadyRequests[i]), MPI_STATUS_IGNORE);
		MPI_Irecv(&(buReadyMessages[i]),sizeof(MpiBuReadyMsg),MPI_CHAR,busRanks[i],MPI_ANY_TAG,MPI_COMM_WORLD,&(buReadyRequests[i]));		
	}	

};

void ManagerUnit :: initializeReceivingFus()
{

	//printf("MU initializing: NUMBER OF fus  %d\n",howManyFus);
	
	MpiFuReadyMsg fuReadyMessage;
	
	//first round to inir receiving
	for ( int i = 0 ; i < howManyFus ; i++)
	{	
		MPI_Wait(&(fuReadyRequests[i]), MPI_STATUS_IGNORE);
		MPI_Irecv(&(fuReadyMessages[i]),sizeof(MpiFuReadyMsg),MPI_CHAR,fusRanks[i],MPI_ANY_TAG,MPI_COMM_WORLD,&(fuReadyRequests[i]));
	}

	//now get all messages and reinit request
	for ( int i = 0 ; i < howManyFus ; i++)
	{

		for ( int j = 0 ; j < FU_ON_THE_GO_SENDS ; j++)
		{
			//printf("MU initializing receive for fu rank  %d and index %d\n",i+1, i);
			//printf("MU initializing receive for fu rank  %d and index %d\n",fusRanks[i], i);
			//MPI_Recv(&fuReadyMessage,sizeof(MpiFuReadyMsg),MPI_CHAR,i+1,MPI_ANY_TAG,MPI_COMM_WORLD,&aStatus);	
			//MPI_Recv(&fuReadyMessages[i],sizeof(MpiFuReadyMsg),MPI_CHAR,fusRanks[i],MPI_ANY_TAG,MPI_COMM_WORLD,&aStatus);	
			MPI_Wait(&(fuReadyRequests[i]), MPI_STATUS_IGNORE);
			MPI_Irecv(&(fuReadyMessages[i]),sizeof(MpiFuReadyMsg),MPI_CHAR,fusRanks[i],MPI_ANY_TAG,MPI_COMM_WORLD,&(fuReadyRequests[i]));
			//MPI_Wait(&(fuReadyRequests[i]), MPI_STATUS_IGNORE);
			fusRanksReady[i]++;		
		}
		//printf("MU got all %d ready requests from FU %d out of %d ALL FUs. fusRanksReady: %d\n",FU_ON_THE_GO_SENDS,i+1,howManyFus,fusRanksReady[i]);
	}
	//printf("MU done  with ready reqests init !\n");
/*
	for ( int i = 0 ; i < howManyFus ; i++)
	{

			//printf("MU initializing receive for fu rank  %d and index %d\n",i+1, i);
			//printf("MU initializing receive for fu rank  %d and index %d\n",fusRanks[i], i);
			//MPI_Recv(&fuReadyMessage,sizeof(MpiFuReadyMsg),MPI_CHAR,i+1,MPI_ANY_TAG,MPI_COMM_WORLD,&aStatus);	
			//MPI_Recv(&fuReadyMessages[i],sizeof(MpiFuReadyMsg),MPI_CHAR,fusRanks[i],MPI_ANY_TAG,MPI_COMM_WORLD,&aStatus);	
			MPI_Wait(&(fuReadyRequests[i]), MPI_STATUS_IGNORE);
			MPI_Irecv(&(fuReadyMessages[i]),sizeof(MpiFuReadyMsg),MPI_CHAR,fusRanks[i],MPI_ANY_TAG,MPI_COMM_WORLD,&(fuReadyRequests[i]));
			//MPI_Wait(&(fuReadyRequests[i]), MPI_STATUS_IGNORE);
			fusRanksReady[i]++;		
		//printf("MU got all %d ready requests from FU %d out of %d ALL FUs. fusRanksReady: %d\n",FU_ON_THE_GO_SENDS,i+1,howManyFus,fusRanksReady[i]);
	}
*/
};	


void ManagerUnit :: getEventsBus()
{
	
	int recv_flag = false;
	int recv_index;
	

	
	MPI_Testany(howManyBus,buReadyRequests,&recv_index, &recv_flag,MPI_STATUS_IGNORE);	
		
	if(recv_flag == true)
	{
		//printf("MU GOt info: Ready BU under index %d corresponding to rank %d. RANK FROM MESSAGE: %d IST CURRENT QUEUE LENGTH: %d. ONCREEMENTING EVENTS NO \n",recv_index, busRanks[recv_index], buReadyMessages[recv_index].rank,buReadyMessages[recv_index].currentQueueLength);
		
		MPI_Wait(&(buReadyRequests[recv_index]), MPI_STATUS_IGNORE);
		MPI_Irecv(&(buReadyMessages[recv_index]),sizeof(MpiBuReadyMsg),MPI_CHAR,busRanks[recv_index],MPI_ANY_TAG,MPI_COMM_WORLD,&(buReadyRequests[recv_index]));	
		//busRanksReady[recv_index] = buReadyMessages[recv_index].currentQueueLength;
		busRanksReady[recv_index]++;
		
		//busRanksReady[recv_index]++;

		//for(int y=0;y!=howManyFus;y++)
		//	printf("MU ready slots for RU rank %d: %d \n",fusRanks[y], fusRanksReady[y]);	
	}
	
};


void ManagerUnit :: sendBusTransmisisonGrant(int buInd, int fuInd)
{
	servicedBuTransmissions++;
	
	transGrantMessages[buInd].sendToWhichFu = fusRanks[fuInd];
	MPI_Wait(&(transGrantRequests[buInd]), MPI_STATUS_IGNORE);
	MPI_Isend(&(transGrantMessages[buInd]),sizeof(MpiBuTeansmissionGrantMsg),MPI_CHAR,busRanks[buInd],rank,MPI_COMM_WORLD,&(transGrantRequests[buInd]));	
	
	//printf("MU Done sending transmission GRANT to BU rank %d and FU rank %d\n",busRanks[buInd],fusRanks[fuInd]);
	
}	

