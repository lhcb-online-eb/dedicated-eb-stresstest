BINDIR   = bin
OBJDIR   = obj

all: makedirs Unit.o BuilderUnit.o FilterUnit.o ManagerUnit.o main.o common.o 
	mpicxx -o bin/benchmark obj/Unit.o obj/FilterUnit.o obj/BuilderUnit.o obj/ManagerUnit.o obj/main.o obj/common.o  -Wall `pkg-config --libs glib-2.0` -O2
Unit.o: src/Unit.cpp
	mpicxx -o obj/Unit.o src/Unit.cpp `pkg-config --cflags glib-2.0` -c -O2 -Wall
FilterUnit.o: src/FilterUnit.cpp
	mpicxx -o obj/FilterUnit.o src/FilterUnit.cpp `pkg-config --cflags glib-2.0` -c -O2 -Wall
ManagerUnit.o: src/ManagerUnit.cpp
	mpicxx -o obj/ManagerUnit.o src/ManagerUnit.cpp `pkg-config --cflags glib-2.0`  -c -O2 -Wall
BuilderUnit.o: src/BuilderUnit.cpp
	mpicxx -o obj/BuilderUnit.o src/BuilderUnit.cpp `pkg-config --cflags glib-2.0`  -c -O2 -Wall
main.o: src/main.cpp
	mpicxx -o obj/main.o src/main.cpp -c -O2 -Wall `pkg-config --cflags glib-2.0`  -c -O2 -Wall
common.o: src/common.cpp
	mpicxx -o obj/common.o src/common.cpp -c -O2
clean:
	rm bin/benchmark
	rm obj/*
makedirs:
ifneq ("$(wildcard $(OBJDIR))","")
	@echo "object folder exists"
else
	mkdir $(OBJDIR)
endif
ifneq ("$(wildcard $(BINDIR))","")
	@echo "binary folder exists"
else
	mkdir $(BINDIR)
endif

