#include <mpi.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <malloc.h>
#include <assert.h>
#include <ctime>
#include <vector>
#include "../inc/Unit.hpp"
#include "../inc/common.hpp"
#include <tuple>
#include <glib.h>


#ifndef FILTERUNIT_HPP
#define FILTERUNIT_HPP

	
class FilterUnit : public Unit
{
	public: 
		FilterUnit(int rank);
		virtual void run();
	private:
		void signalReadiness();
		bool probeForCompletion(int *indx);
		void recvFromBuAtInd(int index);
		//void recvFromBu();
		void initRecvFromBu();
	private:
		char ** dataBuffer;

		//MPI_Request * fuReadyRequests;
		//MPI_Request * dataSendRequests;
		//MPI_Status * fuReadyStatus;
		//MPI_Status * dataSendStatus;
		MPI_Request fuReadyRequests;
		//MPI_Request dataSendRequests;
		//MPI_Status fuReadyStatus;
		//MPI_Status  dataSendStatus;
		MpiFuReadyMsg fuReadyMessage;

		int transmittedChunks;

		//MPI_Request dataSendRequests;
		MPI_Request * dataSentRequests;
		
};

	
#endif



