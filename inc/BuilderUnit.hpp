#include <mpi.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <malloc.h>
#include <assert.h>
#include <ctime>
#include "../inc/Unit.hpp"
#include "../inc/common.hpp"

#ifndef BUILDER_HPP
#define BUILDER_HPP
	
class BuilderUnit : public Unit
{
   public:
		BuilderUnit(int rank); 
		virtual void run();
   private:
		bool generateEventData(); //return true if queue full 
		void sendDataToFu(int rank);
		void signalEventsReady();
		bool timeElapsedCheck();
		int totalEventsOnQueue;
		bool checkIfGrantedTransmission(int * readyRank);
		void startReceiveFromMU();
		int getNextFreeTransmissionIndex();
		char ** dataBuffer;
		bool * transmissionRunningMap;
		int runningBuRuTransmissions;
		
		int64_t now;
		int64_t start;
		int64_t delayTimer;              
		
		MPI_Request buReadyRequests;
		//MPI_Status buReadyStatus;
		MpiBuReadyMsg buReadyMessage;
		
		MPI_Request transGrantedRequests;
		//MPI_Status transGrantedStatus;
		MpiBuTeansmissionGrantMsg transGrantedMessage;
		
		MPI_Request * dataTransmissionRequests;
		

};


#endif

