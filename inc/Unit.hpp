#include <mpi.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <malloc.h>
#include <assert.h>
#include <ctime>
#include "../inc/common.hpp"
#include <glib.h>
#include <limits>
#include <thread>

#ifndef UNIT_HPP
#define UNIT_HPP

	
class Unit
{
	public: 
		Unit(int rank);
		virtual void run();
	protected: 
		int rank;
		unsigned int currentBuffInd;
		unsigned int getNextBuffIndex();
};

	
#endif
