#include <mpi.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <malloc.h>
#include <assert.h>
#include <ctime>
#include <sys/timerfd.h>


#ifndef COMMON_HPP
#define COMMON_HPP


//half a second interval value for builders random wait
const unsigned int RAND_MS = 1000;

//total buffer entries to be sent to builder units
const unsigned int BUFFER_COUNT = 64;

//sending period in microseconds
const unsigned int PERIOD = 10000;

//how many times repeat the transmissions
const unsigned int REPEAT_FOR_EVAL = 4096;

//time lapse in seconds to display BUs throughput
const unsigned int PROBE_TP_LAPSE = 2;

//parameter to set sending to local RUs
const bool SEND_TO_LOCAL = false; 

//MAX parallel BU sends
const int MAX_BU_SENDS = 1; 


const size_t FU_ON_THE_GO_SENDS = 1;

////////////

extern int howManyFus;
extern int howManyBus;
extern int howManyProcesses;

extern unsigned int eventsGeneratedPerBu;


extern int totalProcessesNo;

extern int timeoutSeconds;

struct MpiFuReadyMsg
{
	int rank;
};


struct MpiBuReadyMsg
{
	int rank;
	int currentQueueLength;
};

struct MpiBuTeansmissionGrantMsg
{
	int sendToWhichFu;
};

//once every 50 ms
//200 events per second
//once in how many microseconds to generate new event for the BU
const int64_t TIME_ELAPSED_FOR_GENERATION = 50000;

//buffer size for single data portion in readout-to-builder units transmission
extern size_t BUFFER_SIZE_MB;

//once in how many iterations probe time
const unsigned int PROBE_EVERY_IT = 10000;


#endif



