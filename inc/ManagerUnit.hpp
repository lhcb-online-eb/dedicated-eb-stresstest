#include <mpi.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <malloc.h>
#include <assert.h>
#include <ctime>
#include "../inc/Unit.hpp"
#include "../inc/common.hpp"
#include <glib.h>
#include <limits>
#include <thread>

#ifndef MANAGER_HPP
#define MANAGER_HPP

	
class ManagerUnit: public Unit
{
	public: 
		ManagerUnit(int rank);
		ManagerUnit(int rank,int howmanyFus,int howManyBus);
		virtual void run();
	private:
		void getReadyFus();	
		void getEventsBus();
		void grantTransmissionsToBus();
		void initializeReceivingFus();

		void initializeReceivingBus();
		void sendBusTransmisisonGrant(int buInd, int fuInd);
		int howManyBus;
		int howManyFus;
		
		int servicedBuTransmissions;

		//lookups for ranks that BUs have - goes togethet with busRanksReady 
		int * busRanks;
		int * fusRanks;
			
		//counters of ready BUs events packets
		int * busRanksReady;
		int * fusRanksReady;
		
		MPI_Request * fuReadyRequests;
		MPI_Request * buReadyRequests;
		//MPI_Status * fuReadyStatus;
		//MPI_Status * buReadyStatus;
		
		MPI_Request * transGrantRequests;
		//MPI_Status * transGrantStatus;
		
		
		MpiFuReadyMsg * fuReadyMessages;
		MpiBuReadyMsg * buReadyMessages;

		MpiBuTeansmissionGrantMsg * transGrantMessages;
};

	
#endif




