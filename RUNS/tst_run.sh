#!/bin/bash

params1="
-x UCX_NET_DEVICES=mlx5_0:1
-x UCX_IB_GID_INDEX=3
-x UCX_TLS=rc_x,sm,self
-bind-to none
--mca coll ^hcoll
--mca pml ucx 
--mca btl ^openib"

params2="
-x UCX_NET_DEVICES=mlx5_1:1 
-x UCX_IB_GID_INDEX=3
-x UCX_TLS=rc_x,sm,self
-bind-to none
--mca coll ^hcoll
--mca pml ucx 
--mca btl ^openib"

params3="
-x UCX_NET_DEVICES=mlx5_3:1 
-x UCX_IB_GID_INDEX=3
-x UCX_TLS=rc_x,sm,self
-bind-to none
--mca coll ^hcoll
--mca pml ucx 
--mca btl ^openib"


params_pfc_ecn="
-x UCX_RC_VERBS_TRAFFIC_CLASS=24
-x UCX_RC_MLX5_TRAFFIC_CLASS=24
-x UCX_DC_VERBS_TRAFFIC_CLASS=24
-x UCX_DC_MLX5_TRAFFIC_CLASS=24
-x UCX_UD_VERBS_TRAFFIC_CLASS=24
-x UCX_UD_MLX5_TRAFFIC_CLASS=24
-x UCX_RC_VERBS_SL=3
-x UCX_RC_MLX5_SL=3
-x UCX_DC_MLX5_SL=3
-x UCX_UD_VERBS_SL=3
-x UCX_UD_MLX5_SL=3
-x UCX_CM_SL=3
"


tt=10
	
runargs="-t $tt"

#$MPI_ROOT/tests/osu-micro-benchmarks-5.3.2/osu_hello


#wrapper_intel="hwloc-bind --membind node:0 --cpubind node:0 /home/rkrawczy/CERN_18/benchmarks_rk/bin/one_to_one -n 1"
#wrapper_amd="hwloc-bind --membind node:1 --cpubind node:1 /home/rkrawczy/CERN_18/benchmarks_rk/bin/one_to_one -n 1"



#wrapper_intel="hwloc-bind --membind node:0 --cpubind node:0 /usr/mpi/gcc/openmpi-4.0.2a1/tests/osu-micro-benchmarks-5.3.2/osu_get_bw"
#wrapper_amd="hwloc-bind --membind node:1 --cpubind node:1 /usr/mpi/gcc/openmpi-4.0.2a1/tests/osu-micro-benchmarks-5.3.2/osu_get_bw"

#wrapper_intel="hwloc-bind --membind node:0 --cpubind node:0 ./a.out -n 1"
#wrapper_amd="hwloc-bind --membind node:1 --cpubind node:1 ./a.out -n 1"
	
wrapper_intel="hwloc-bind --membind node:0 --cpubind node:0 /home/rkrawczy/CERN_19/DEDICATED_EB/dedicated-eb-stresstest/bin/benchmark $runargs" 
wrapper_amd="hwloc-bind --membind node:1 --cpubind node:1 /home/rkrawczy/CERN_19/DEDICATED_EB/dedicated-eb-stresstest/bin/benchmark $runargs" 


#mpirun -n 1 -npernode 1 --hostfile ./HOSTFILES/hosts_amd_tst $params2 $wrapper_amd -F 6 : -n 1 -npernode 1 --hostfile ./HOSTFILES/hosts_amd_tst $params2 $wrapper_amd -F 6 : -n 2 -npernode 1 --hostfile ./HOSTFILES/hosts_hltv03_tst $params1 $wrapper_intel -F 6 : -n 2 -npernode 1 --hostfile ./HOSTFILES/hosts_hltv03_tst $params1 $wrapper_intel -F 6 : -n 2 -npernode 1 --hostfile ./HOSTFILES/hosts_hltv03_tst $params1 $wrapper_intel -F 6

#mpirun -n 1 -npernode 1 --hostfile ./HOSTFILES/hosts_amd_tst $params2 $wrapper_amd -F 3 : -n 1 -npernode 1 --hostfile ./HOSTFILES/hosts_amd_tst $params2 $wrapper_amd -F 3 : -n 1 -npernode 1 --hostfile ./HOSTFILES/hosts_hltv03_tst_2 $params1 $wrapper_intel -F 3 : -n 1 -npernode 1 --hostfile ./HOSTFILES/hosts_hltv03_tst_2 $params1 $wrapper_intel -F 3 : -n 1 -npernode 1 --hostfile ./HOSTFILES/hosts_hltv03_tst_1 $params1 $wrapper_intel -F 3

#mpirun -n 1 -npernode 1 --hostfile ./HOSTFILES/hosts_amd_tst $params2 $wrapper_amd -F 8 : -n 2 -npernode 1 --hostfile ./HOSTFILES/hosts_amd_tst_2 $params2 $wrapper_amd -F 8 : -n 8 -npernode 1 --hostfile ./HOSTFILES/hosts_hltv03_tst_4 $params1 $wrapper_intel -F 8

#mpirun -n 1 -npernode 1 --hostfile ./HOSTFILES/hosts_amd_tst $params2 $wrapper_amd -F 4 : -n 2 -npernode 1 --hostfile ./HOSTFILES/hosts_amd_tst_2 $params2 $wrapper_amd -F 4 : -n 4 -npernode 1 --hostfile ./HOSTFILES/hosts_hltv03_tst_5 $params1 $wrapper_intel -F 4

#mpirun -n 1 -npernode 1 --hostfile ./HOSTFILES/hosts_hltv03_tst_1 $params1 $wrapper_intel -F 6 : -n 2 -npernode 1 --hostfile ./HOSTFILES/hosts_hltv03_tst_2 $params1 $wrapper_intel -F 6 : -n 6 -npernode 1 --hostfile ./HOSTFILES/hosts_hltv03_tst_5 $params1 $wrapper_intel -F 6

#mpirun -n 8 -npernode 1 --hostfile ./HOSTFILES/hosts_hltv03_tst_5 $params1 $wrapper_intel -F 7

##mpirun -n 1 -npernode 1 --hostfile ./HOSTFILES/hosts_amd_tst $params2 $params_pfc_ecn $wrapper_amd -F 8 : -n 2 -npernode 1 --hostfile ./HOSTFILES/hosts_amd_tst_2 $params2 $params_pfc_ecn $wrapper_amd -F 8 : -n 8 -npernode 1 --hostfile ./HOSTFILES/hosts_hltv03_tst_5 $params1 $params_pfc_ecn $wrapper_intel -F 8

#mpirun -n 1 -npernode 1 --hostfile ./HOSTFILES/hosts_amd_tst $params2 $params_pfc_ecn $wrapper_amd -F 2 : -n 1 -npernode 1 --hostfile ./HOSTFILES/hosts_amd_tst_2 $params2 $params_pfc_ecn $wrapper_amd -F 2 : -n 2 -npernode 1 --hostfile ./HOSTFILES/hosts_hltv03_tst_5 $params1 $params_pfc_ecn $wrapper_intel -F 2

mpirun -n 1 -npernode 1 --hostfile ./HOSTFILES/hosts_amd_tst $params2 $params_pfc_ecn $wrapper_amd -F 1 : -n 1 -npernode 1 --hostfile ./HOSTFILES/hosts_amd_tst_2 $params2 $params_pfc_ecn $wrapper_amd -F 1 : -n 1 -npernode 1 --hostfile ./HOSTFILES/hosts_hltv03_tst_1 $params1 $params_pfc_ecn $wrapper_intel -F 1


#mpirun -n 1 -npernode 1 --hostfile ./HOSTFILES/hosts_amd_tst $params2 $params_pfc_ecn $wrapper_amd $-F 1 : -n 1 -npernode 1 --hostfile ./HOSTFILES/hosts_amd_tst_2 $params2 $params_pfc_ecn $wrapper_amd -F 1 : -n 1 -npernode 1 --hostfile ./HOSTFILES/hosts_hltv03_tst_5 $params1 $params_pfc_ecn $wrapper_intel -F 1

#mpirun -n 1 -npernode 1 --hostfile ./HOSTFILES/hosts_amd_tst $params2 $params_pfc_ecn $wrapper_amd $-F 1 : -n 1 -npernode 1 --hostfile ./HOSTFILES/hosts_amd_tst_2 $params2 $params_pfc_ecn $wrapper_amd -F 1 : -n 1 -npernode 1 --hostfile ./HOSTFILES/hosts_hltv03_tst_5 $params1 $params_pfc_ecn $wrapper_intel -F 1



#mpirun -n 1 -npernode 1 --hostfile ./HOSTFILES/hosts_amd_tst $params2 $wrapper_amd  : -n 2 -npernode 1 --hostfile ./HOSTFILES/hosts_amd_tst_2 $params2 $wrapper_amd  : -n 6 -npernode 1 --hostfile ./HOSTFILES/hosts_hltv03_tst_5 $params1 $wrapper_intel 

#mpirun -n 6 -npernode 1 --hostfile ./HOSTFILES/hosts_hltv03_tst_5 $params1 $wrapper_intel 


#mpirun -n 1 -npernode 1 --hostfile ./HOSTFILES/hosts_amd_tst $params2 $wrapper_amd -F 3 : -n 2 -npernode 1 --hostfile ./HOSTFILES/hosts_amd_tst_2 $params2 $wrapper_amd -F 3 : -n 1 -npernode 1 --hostfile ./HOSTFILES/hosts_hltv03_tst_2 $params1 $wrapper_intel -F 3 : -n 1 -npernode 1 --hostfile ./HOSTFILES/hosts_hltv03_tst_2 $params1 $wrapper_intel -F 3 : -n 1 -npernode 1 --hostfile ./HOSTFILES/hosts_hltv03_tst_1 $params1 $wrapper_intel -F 3


#mpirun -n 1 -npernode 1 --hostfile ./HOSTFILES/hosts_amd_tst $params2 $wrapper_amd -F 2 : -n 1 -npernode 1 --hostfile ./HOSTFILES/hosts_amd_tst $params2 $wrapper_amd -F 2 : -n 1 -npernode 1 --hostfile ./HOSTFILES/hosts_hltv03_tst_2 $params1 $wrapper_intel -F 2 : -n 1 -npernode 1 --hostfile ./HOSTFILES/hosts_hltv03_tst_2 $params1 $wrapper_intel -F 2


#mpirun -n 1 -npernode 1 --hostfile ./HOSTFILES/hosts_amd_tst $params2 $wrapper_amd -F 2 : -n 1 -npernode 1 --hostfile ./HOSTFILES/hosts_amd_tst $params2 $wrapper_amd -F 2 : -n 2 -npernode 1 --hostfile ./HOSTFILES/hosts_hltv03_tst $params1 $wrapper_intel -F 2

#mpirun -n 1 -npernode 1 --hostfile ./HOSTFILES/hosts_amd_tst $params2 $wrapper_amd -F 2 : -n 1 -npernode 1 --hostfile ./HOSTFILES/hosts_amd_tst $params2 $wrapper_amd -F 2 : -n 1 -npernode 1 --hostfile ./HOSTFILES/hosts_hltv03_tst_1 $params1 $wrapper_intel -F 2 : -n 1 -npernode 1 --hostfile ./HOSTFILES/hosts_hltv03_tst_1 $params1 $wrapper_intel -F  2



#mpirun -n 1 -npernode 1 --hostfile ./HOSTFILES/hosts_amd_tst $params2 $wrapper_amd -F 4 : -n 1 -npernode 1 --hostfile ./HOSTFILES/hosts_amd_tst $params2 $wrapper_amd -F 4 : -n 2 -npernode 1 --hostfile ./HOSTFILES/hosts_hltv03_tst $params1 $wrapper_intel -F 4  : -n 2 -npernode 1 --hostfile ./HOSTFILES/hosts_hltv03_tst $params1 $wrapper_intel -F 4


#mpirun -n 1 -npernode 1 --hostfile ./HOSTFILES/hosts_amd_tst $params3 $wrapper_amd -F 1 : -n 1 -npernode 1 --hostfile ./HOSTFILES/hosts_amd_tst $params3 $wrapper_amd -F 1 : -n 1 -npernode 1 --hostfile ./HOSTFILES/hosts_hltv03_tst $params1 $wrapper_intel -F 1


#mpirun -n 1 -npernode 1 --hostfile ./HOSTFILES/hosts_hltv03_tst $params3 $wrapper_intel -F 4 : -n 1 -npernode 1 --hostfile ./HOSTFILES/hosts_hltv03_tst $params3 $wrapper_intel -F 2 : -n 2 -npernode 1 --hostfile ./HOSTFILES/hosts_hltv03_tst $params1 $wrapper_intel -F 4  : -n 2 -npernode 1 --hostfile ./HOSTFILES/hosts_hltv03_tst $params1 $wrapper_intel -F 4

#mpirun -n 1 -npernode 1 --hostfile ./HOSTFILES/hosts_amd_tst $params3 $wrapper_amd -F 4 : -n 1 -npernode 1 --hostfile ./HOSTFILES/hosts_amd_tst $params3 $wrapper_amd -F 2 : -n 2 -npernode 1 --hostfile ./HOSTFILES/hosts_hltv03_tst $params1 $wrapper_intel -F 4  : -n 2 -npernode 1 --hostfile ./HOSTFILES/hosts_hltv03_tst $params1 $wrapper_intel -F 4

#mpirun -n 1 -npernode 1 --hostfile ./HOSTFILES/hosts_hltv03_tst_1 $params1 $wrapper_intel : -n 1 -npernode 1 --hostfile ./HOSTFILES/hosts_hltv03_tst_1 $params1 $wrapper_intel


#hosts_hltv03_tst_1
#mpirun -n 1 -npernode 1 --hostfile ./HOSTFILES/hosts_amd_tst $params2 $params_pfc_ecn $wrapper_amd : -n 1 -npernode 1 --hostfile ./HOSTFILES/hosts_hltv03_tst_1 $params1 $wrapper_intel 


#mpirun -n 1 -npernode 1 --hostfile ./HOSTFILES/hosts_amd_tst $params3 $wrapper_amd -F 1 : -n 1 -npernode 1 --hostfile ./HOSTFILES/hosts_amd_tst $params3 $wrapper_amd -F 1 : -n 1 -npernode 1 --hostfile ./HOSTFILES/hosts_amd_tst $params1 $wrapper_amd -F 1 

#mpirun -n 1 -npernode 1 --hostfile ./HOSTFILES/hosts_amd_tst $params2 $wrapper_amd -F 1 : -n 1 -npernode 1 --hostfile ./HOSTFILES/hosts_hltv03_tst_1 $params1 $wrapper_intel -F 1 : -n 1 -npernode 1 --hostfile ./HOSTFILES/hosts_hltv03_tst_2 $params1 $wrapper_intel -F 1


